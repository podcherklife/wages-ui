import {Container, Image, Menu} from "semantic-ui-react";
import React from "react";

export default function Navbar () {
    return(
        <Menu fixed='top' inverted>
            <Container>
                <Menu.Item as='a' href="/" header>
                    <Image size='mini' src='/logo192.png' style={{ marginRight: '1.5em' }} />
                    Waves
                </Menu.Item>
                <Menu.Item as='a' href="/processes">Processes</Menu.Item>
            </Container>
        </Menu>
    )
}