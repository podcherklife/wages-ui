import React from "react";
import { RouteComponentProps } from "react-router";

interface MatchParams {
    id: string
}

export default function ProcessPage({match}: RouteComponentProps<MatchParams>) {
    const {id} = match.params;
    return (
        <div>Process id: {id}</div>
    );
}