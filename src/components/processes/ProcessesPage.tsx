import * as Apollo from '@apollo/client';
import * as React from 'react';
import {
    Button, Header,
    Icon, Loader,
    Progress, Table
} from 'semantic-ui-react';
import * as Q from '../../generated/graphql';

export default function ProcessesPage() {
    const { loading, error, data } = Apollo.useQuery<Q.ProcessesQuery, Q.ProcessesQueryVariables>(Q.ProcessesDocument);
    if (loading) return (
        <Loader active inline='centered' >Loading...</Loader>
    );
    if (error) return <div> Error: {error} </div>;

    const processLines = data?.processes?.map((process) => <ProcessRow key={process.id} {...process} />);

    return (<>
        <Header as='h1'>
            <Icon name='settings' />
            <Header.Content>
                Processes ({data?.processes.reduce((a, e) => (e.state?.details?.working ? 1 : 0) + a, 0)} running)
                <Header.Subheader>Manage your processes</Header.Subheader>
            </Header.Content>
        </Header>
        <Table definition>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell />
                    <Table.HeaderCell singleLine>Id</Table.HeaderCell>
                    <Table.HeaderCell>Description</Table.HeaderCell>
                    <Table.HeaderCell>Status</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {processLines}
            </Table.Body>
        </Table>
    </>
    );
}

function availableActionsForProcess(status?: Q.ProcessStatus): Q.ProcessAction[] {
    if (status == Q.ProcessStatus.NoActiveExecutions) {
        return [Q.ProcessAction.CreateNewOrContinueFailedExecution];
    } else if (status == Q.ProcessStatus.ExecutionPending) {
        return [Q.ProcessAction.StartPendingExecution, Q.ProcessAction.CancelPendingExecution];
    } else if (status == Q.ProcessStatus.ExecutionInProgress) {
        return [Q.ProcessAction.TryStopExecutionInProgress];
    } else {
        return [];
    }
}

interface ProcessManagementButtonProps {
    processId: string,
    action: Q.ProcessAction,
    disabled: boolean,
}

const ProcessManagementButton: React.FunctionComponent<ProcessManagementButtonProps> = ({ processId, action, disabled, children }) => {
    const [executeOperation, { data }] = Apollo.useMutation<Q.ManageProcessMutation>(Q.ManageProcessDocument, {
        variables: { id: processId, action: action.toString() }
    });

    return <Button disabled={disabled} onClick={() => executeOperation()}>{children}</Button>;
}

function ProcessManagementButtons({ process }: { process: Q.ProcessesQuery['processes'][0] }) {
    const availableActions = availableActionsForProcess(process.state?.generalStatus);

    const [doQuery, fetchProcessStateResult] = Apollo.useLazyQuery<Q.ProcessesQuery, Q.ProcessesQueryVariables>(Q.ProcessesDocument);

    const refreshProcessState = React.useCallback(() => {
        if (fetchProcessStateResult.refetch) {
            fetchProcessStateResult.refetch({ id: process.id })
        } else {
            doQuery({ variables: { id: process.id, /*v: Math.random().toString() */ } });
        }
    }, [fetchProcessStateResult]);
    return <Button.Group icon>
        <ProcessManagementButton
            disabled={!availableActions.includes(Q.ProcessAction.CreateNewOrContinueFailedExecution)}
            processId={process.id}
            action={Q.ProcessAction.CreateNewOrContinueFailedExecution}>
            <Icon name='play circle outline' />
        </ProcessManagementButton>
        <ProcessManagementButton
            disabled={!availableActions.includes(Q.ProcessAction.StartPendingExecution)}
            processId={process.id}
            action={Q.ProcessAction.StartPendingExecution}>
            <Icon name='play circle outline' />
        </ProcessManagementButton>
        <ProcessManagementButton
            disabled={!availableActions.includes(Q.ProcessAction.CancelPendingExecution)}
            processId={process.id}
            action={Q.ProcessAction.CancelPendingExecution}>
            <Icon name='cancel' />
        </ProcessManagementButton>
        <Button onClick={refreshProcessState}>
            <Icon name='refresh' />
        </Button>
    </Button.Group>
}


function ProcessRow(process: Q.ProcessesQuery['processes'][number]) {
    return (
        <Table.Row>
            <Table.Cell collapsing>
                <ProcessManagementButtons process={process} />
            </Table.Cell>
            <Table.Cell width={3}>
                <a href={`process/${process.id}`}>{process.id}</a>
            </Table.Cell>
            <Table.Cell>
                {process.state?.details?.description}
            </Table.Cell>
            <Table.Cell width={2}>
                {process.state?.details &&
                    <Progress percent={process.state?.details.percentsCompleted} progress active={process.state.details?.working} />
                }
            </Table.Cell>
        </Table.Row>
    )
}