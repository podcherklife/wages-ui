import React from 'react';
import {
    Container,
    Header
} from 'semantic-ui-react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'

import Navbar from './components/Navbar';
import Footer from './components/Footer';
import ProcessesPage from './components/processes/ProcessesPage';
import ProcessPage from './components/processes/ProcessPage';

function App() {
  return (
      <div>

          <Router>
              <Navbar />
              <Container style={{ marginTop: '7em' }}>
                  <Switch>
                      <Route exact path="/" render={() => (
                          <Container text>
                              <Header as='h1'>Semantic UI React Fixed Template</Header>
                              <p>This is a basic fixed menu template using fixed size containers.</p>
                              <p>
                                  A text container is used for the main container, which is useful for single column layouts.
                              </p>
                          </Container>
                      )} />
                      <Route exact path="/processes" component={ProcessesPage} />
                      <Route exact path="/process/:id" component={ProcessPage} />
                  </Switch>
              </Container>
              <Footer />
          </Router>
      </div>

  );
}

export default App;
