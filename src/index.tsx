import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import 'semantic-ui-css/semantic.min.css';

const uri = process.env.REACT_APP_SCHEMA_PATH || `http://localhost:8080/graphql`;

if (!process.env.REACT_APP_SCHEMA_PATH)
  console.warn(`Will used default process.env.REACT_APP_SCHEMA_PATH.`);

const client = new ApolloClient({
  uri,
  cache: new InMemoryCache()
});

ReactDOM.render(
    <ApolloProvider client={client}>
      <App/>
    </ApolloProvider>,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
